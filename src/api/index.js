import axios from "axios";
import dotenv from "dotenv";
dotenv.config();

export const getUser = async () => {

  await axios.get("https://api.cryptoplease.me/auth/twitch/user")
  .then((res) => {

    console.log(res.data);
  })
  .catch((error) => {
    console.log(error);
  });
}

export const saveWallet = async (username, wallet, memo) => {

  const res = await axios.post("https://api.cryptoplease.me/users/wallet",
    { 
      username: username,
      wallet: wallet,
      memo: memo
    }
  );

  console.log(res.data);
}

export const depositConfirm = async (memo, username) => {
  const res = await axios.post("https://api.cryptoplease.me/crypto/deposit", {
    memo: memo,
    username: username
  })
  console.log(res)

  return res;
}

export const getUserInfo = async (username) => {
  const res = await axios.post("https://api.cryptoplease.me/users", 
    {
      username: username
    }
  );
  console.log(res);

  return res;
}

export const temp = async(url) => {
  const res = await axios.get(url)
  console.log(res)

}

export const getAccessCode = async(accessCode)=> {
  const res = await axios.post("https://api.cryptoplease.me/auth/twitch/callback", {
    accessCode: accessCode
  })

  return res;
}

export const donationTrigger = async (username, amount, message) => {
  const res = await axios.post("https://api.cryptoplease.me/crypto/send",
    {
      username: username,
      amount: amount,
      message: message
    }
  );

  return res;
}
