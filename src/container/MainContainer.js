import React, { useState, useEffect, useLocation } from 'react';
import { Route, withRouter } from 'react-router-dom';
import MainPage from '../components/MainPage';
import StreamerPage from '../components/StreamerPage';
import WalletPage from '../components/WalletPage';
import DepositPage from '../components/DepositPage';
import DonationPage from '../components/DonationPage';
import AccountPage from '../components/AccountPage';
import Header from '../components/HeaderPage';
import theme from '../components/ui/Theme';
import { ThemeProvider } from "@material-ui/styles";
import { saveWallet, getUserInfo, getAccessCode, donationTrigger } from "../api";
import io from 'socket.io-client';
import dotenv from 'dotenv';
dotenv.config();

function MainContainer(props) {
  const [text, setText] = useState(null);
  const [quantity, setQuantity] = useState(null);
  const [submit, setSubmit] = useState(false);
  const [username, setUsername] = useState(null);
  const [url, setUrl] = useState("");
  const [wallet, setWallet] = useState("");
  const [savedwallet, setSavedWallet] = useState("");
  const [memo, setMemo] = useState("");
  const [headerHide, setHeaderHide] = useState(false);
  const [viewer, setViewer] = useState(false);
  const [streamer, setStreamer] = useState(false);
  const [buttonClick, setButtonClick] = useState(false);
  const [accessCode, setAccessCode] = useState("");
  const [deposit, setDeposit] = useState(null);
  const [depositCheck, setDepositCheck] = useState("");
  const [sendTo, setSendTo] = useState(null);
  const [donationData, setDonationData] = useState(1111);

  useEffect(() => {
    if (text) {
      (async () => { 
        const res = await donationTrigger(sendTo, quantity, text);
        sessionStorage.setItem("deposit", res.data.deposit);
      })();
    }
  }, [text || sendTo])

  useEffect(() => {
    if (!sessionStorage.username || streamer || viewer || props.location.search) {
      (async () => {
        const res = await getAccessCode(props.location.search.slice(6, 36));
        setUsername(res.data);
      })();
    }
  }, [props.location.search])

  useEffect(() => {
    if (username) {
      (async () => {
        const res = await getUserInfo(username);
        sessionStorage.setItem("username", res.data.username);
        sessionStorage.setItem("streamerWallet", res.data.address);
        sessionStorage.setItem("streamerMemo", res.data.memo);
        sessionStorage.setItem("url", res.data.url);
        sessionStorage.setItem("deposit", res.data.deposit);
        setWallet(res.data.address);
        setMemo(res.data.memo);
        setUrl(res.data.url);
        setDeposit(res.data.deposit);
      })();
    }
    setUrl(Math.random().toString(36).substring(2, 15));
  }, [username]);


  useEffect(() => {
    if (streamer || viewer) {

    }
    setUrl(Math.random().toString(36).substring(2, 15));
  }, [streamer || viewer]);

  useEffect(() => {
    if (wallet.length > 0) {
      (async () => { 
        const res = saveWallet(sessionStorage.username, wallet, memo);
      })();
    }
  }, [buttonClick]);

  return (
    <div>
      <Route exact path="/" component={() =>
        <MainPage
          username = { username }
          setStreamer = { setStreamer }
          setViewer = { setViewer }

        />
      }
      />
      <Route exact path="/deposit" component={() =>
        <DepositPage
          setDepositCheck = { setDepositCheck }
          depositCheck = { depositCheck }
          setStreamer = { setStreamer }
          setViewer = { setViewer }
        />
      }
      />
      <Route exact path="/wallet" component={() =>
        <WalletPage
          url = { url }
          setUrl = { setUrl }
          wallet = { wallet }
          memo = { memo }
          setButtonClick = { setButtonClick }
          setWallet = { setWallet }
          setMemo = { setMemo }
          username = { username }
          setStreamer = { setStreamer }
          setViewer = { setViewer }
        />
      }
      />
      <Route exact path="/streamer" component={() =>
        <StreamerPage
          setText = { setText }
          setQuantity = { setQuantity }
          setSubmit = { setSubmit }
          setSendTo = { setSendTo }
          setStreamer = { setStreamer }
          setViewer = { setViewer }
        />
      }
      />
      <Route exact path="/account" component={() =>
        <AccountPage
          setStreamer = { setStreamer }
          setViewer = { setViewer }
        />
      }
      />
      <Route exact path="/donation" component={() =>
        <DonationPage
        />
      }
      />
    </div>
  );
}

export default withRouter(MainContainer);
