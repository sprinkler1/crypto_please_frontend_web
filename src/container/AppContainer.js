import React from 'react';
import './App.css';
import MainContainer from './MainContainer';
import dotenv from 'dotenv';
dotenv.config();

function App() {

  return (
    <MainContainer />
  );
}

export default App;
