import React from "react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MuiAppBar from '@material-ui/core/AppBar';
import Header from '../components/HeaderPage';
import { ThemeProvider } from "@material-ui/styles";
import theme from '../components/ui/Theme';

function AccountPage() {

  return (
    <div className="AccountPage">
      <ThemeProvider theme={theme}>
        <Header />
      </ThemeProvider>
    </div>
  );
}

export default AccountPage;
