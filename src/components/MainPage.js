import React from "react";
import { Cookies, useCookies } from 'react-cookie';
import { AppBar, Toolbar, Typography, Tab, Tabs, Button, Grid } from "@material-ui/core";
import dotenv from "dotenv";
import api from "../api";
import { Link } from 'react-router-dom';
import { saveWallet, getUserInfo, getAccessCode, } from "../api";
import stellarLogo from '../components/ui/stellar-lumen.jpg'
import twitchLogo from '../components/ui/Twitch_logo.svg'
import Header from '../components/HeaderPage';
import { ThemeProvider } from "@material-ui/styles";
import HorizontalLinearStepper from './ui/HorizontalStepper';
import theme from '../components/ui/Theme';
dotenv.config();

function MainPage(props) {
  const twitch_auth = "https://id.twitch.tv/oauth2/authorize?client_id="
    +process.env.REACT_APP_CLIENT_ID+"&redirect_uri="
    +process.env.REACT_APP_REDIRECT_URI+"&response_type=code"
    +"&scope=user_read";

  const isStreamer = (e) => {
    e.preventDefault();
    props.setStreamer(true);
    window.location = twitch_auth;
  }

  const isViewer = (e) => {
    e.preventDefault();
    props.setViewer(true);
    window.location = twitch_auth;
  }

  return (

    <div className="MainPage">
      <ThemeProvider theme={theme}>
        <Header />
      </ThemeProvider>
      <Grid container style={{ padding: '3em 5em' }}>
        <Grid item container direction="column" xs={6}>
          <Grid item>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '3em'}}
            >
            <Typography variant="h5">CRYPTO PLEASE</Typography>
            
            </Grid>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '3em'}}
            >
            <Typography variant="h6">
              Crypto Please is an donation tool for twitch broadcasters.
            </Typography>
            <Typography variant="h6">
              This service currently supports Stellar Lumen(XLM).
            </Typography>
            
            </Grid>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '3em'}}
            >
            { !props.username && !sessionStorage.username?
            <div>
              <Button variant="contained" color="primary" onClick={isStreamer}>STREAMER</Button>
              <Button variant="contained" color="secondary" onClick={isViewer}>VIEWER</Button>
            </div>: null }

            </Grid>

          </Grid>
        </Grid>
        <Grid item container direction="column" xs={6}>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '4em'}}
            >
              <img src={twitchLogo} width={"400px"} heigth={"100px"} />
              <Typography variant="body1">
              </Typography>
            </Grid>
          
        </Grid>
        <HorizontalLinearStepper />
      </Grid>

    </div>
  );
}

export default MainPage;
