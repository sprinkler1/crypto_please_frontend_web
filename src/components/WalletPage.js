import React, { useState, useRef } from "react";
import { TextField, Grid, Typography, Button, FormControl } from '@material-ui/core';
import Header from '../components/HeaderPage';
import { ThemeProvider } from "@material-ui/styles";
import theme from '../components/ui/Theme';

function WallerPage(props) {
  const memoRef = useRef("");
  const walletRef = useRef("");

  const handleClick = () => {
    props.setWallet(walletRef.current.value);
    props.setMemo(memoRef.current.value);
    sessionStorage.setItem("streamerWallet", walletRef.current.value);
    sessionStorage.setItem("streamerMemo", memoRef.current.value);
    props.setButtonClick(true);
  }

  const url = () => {

    return Math.random().toString(36).substring(2, 15);
  }

  return (
    <div className="WallerPage">
      <ThemeProvider theme={theme}>
        <Header />
      </ThemeProvider>
      <Grid container style={{ padding: '3em 5em' }}>
        <Grid item container direction="column" xs={6}>
          <Grid item>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '2em'}}
            >
            <Typography variant="h5">WALLET</Typography>
            </Grid>
              <Grid
                container
                direction="column"
                justify="space-between"
                style={{ padding: '2em'}}
              >
                <Typography variant="h6">WALLET ADDRESS</Typography>
                <FormControl margin="normal" required fullWidth>
                <TextField
                  inputRef={walletRef}
                  onChange={e => {}}
                  placeholder="Ex) GCBR3TZZQWKQTSQD2A3IMXVXF5JK4BWSYZOHJ7JR7YRJRX7C7DU2UNO7"
                  rows={1}
                  required={true}
                />
                </FormControl>
              </Grid>
              <Grid
                container
                direction="column"
                justify="space-between"
                style={{ padding: '2em'}}
              >
                <Typography variant="h6">MEMO</Typography>
                <FormControl margin="normal" required fullWidth>
                <TextField
                  inputRef={memoRef}
                  onChange={e => {}}
                  placeholder="Ex) 2446"
                  rows={1}
                  required={true}
                />
                </FormControl>
              </Grid>
              <Grid
                container
                direction="column"
                justify="space-between"
                style={{ padding: '2em'}}
              >
                <Button onClick={handleClick} variant="contained" color="primary">
                  Save
                </Button>
              </Grid>
          </Grid>
        </Grid>
        <Grid item container direction="column" xs={6}>
          <Grid item>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '2em'}}
            >
            <Typography variant="h5">SAVED WALLET</Typography>
            </Grid>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '2em'}}
            >
              <Typography variant="h6">WALLET ADDRESS</Typography>
              <Typography variant="body1">{sessionStorage.streamerWallet || ""}</Typography>
            </Grid>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '2em'}}
            >
              <Typography variant="h6">MEMO</Typography>
              <Typography variant="body1">{sessionStorage.streamerMemo || ""}</Typography>
            </Grid>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '2em'}}
            >
              <Typography variant="h6">YOUR OBS URL</Typography>
              <Typography variant="body1">
                http://localhost:3000/streamer/{sessionStorage.username || ""}
              </Typography>
            </Grid>
          </Grid>
        </Grid>

      </Grid>
    </div>
  );
}

export default WallerPage;
