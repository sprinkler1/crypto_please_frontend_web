import React, { useState, useRef } from "react";
import { TextField, Grid, Typography, Button, Box } from '@material-ui/core';
import { depositConfirm } from "../api"
import qrcode from "./ui/wallet_qrcode.png";
import blockChain from "./ui/blockchain.gif";
import stellar from "./ui/stellar.svg";
import Lottie from 'react-lottie';
import * as animationData from "./ui/data.json"
import Header from '../components/HeaderPage';
import { ThemeProvider } from "@material-ui/styles";
import theme from '../components/ui/Theme';

function DepositPage(props) {
  const textRef = useRef("");
  const coinRef = useRef("");

  const handleClick = (e) => {
    e.preventDefault();
    (async () => { 
      const res = await depositConfirm(sessionStorage.memo, sessionStorage.username)
      sessionStorage.setItem("deposit", res.data.deposit);
      props.setDepositCheck(res.data.deposit);
    })();
  }

  const memo = () => {
    if (sessionStorage.memo) {

      return sessionStorage.memo;
    } else {
      sessionStorage.setItem("memo", Math.random().toString(36).substring(2, 15));
      return sessionStorage.memo;
    }
  }

  const defaultOptions = {
    loop: true,
    autoplay: true, 
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  return (
    <div className="DepositPage">
      <ThemeProvider theme={theme}>
        <Header />
      </ThemeProvider>
      <Grid container style={{ padding: '3em 5em' }}>
        <Grid item container direction="column" xs={6}>
          <Grid item>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '2em'}}
            >
            <Typography variant="h5">DEPOSIT</Typography>
            </Grid>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '2em'}}
            >
              <Typography variant="h6">DEPOSIT ADDRESS</Typography>
              <img src={qrcode} width={"200px"} height={"200px"} />
              <Typography variant="body1">
                GCBR3TZZQWKQTSQD2A3IMXVXF5JK4BWSYZOHJ7JR7YRJRX7C7DU2UNO7
              </Typography>

            </Grid>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '2em'}}
            >
              <Typography variant="h6">MEMO</Typography>
              <Typography variant="body1">
                {memo()}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item container direction="column" xs={6}>
          <Grid item>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '2em'}}
            >
            <Typography variant="h5">ACCOUNT BALANCE</Typography>
            </Grid>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '2em'}}
            >
            <Typography variant="h6">{sessionStorage.deposit || 0} XLM</Typography>
            </Grid>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '2em'}}
            >
            <Button onClick={ handleClick } variant="contained" color="primary">
              Deposit Confirm
            </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default DepositPage;
