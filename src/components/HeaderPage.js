import React from "react";
import { AppBar, Toolbar, Typography, Tab, Tabs, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import { Link } from 'react-router-dom';
import logo from './ui/Header_logo.png';
import { getUser,saveWallet } from "../api";
import { ThemeProvider } from "@material-ui/styles";
import axios from "axios";

function ElevationScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

const useStyles = makeStyles(theme => ({
  tabContainer: {
    marginLeft: "auto"
  }
}))

function Header(props) {
  const classes = useStyles();
  const twitch_auth = "https://id.twitch.tv/oauth2/authorize?client_id="
    +process.env.REACT_APP_CLIENT_ID+"&redirect_uri="
    +process.env.REACT_APP_REDIRECT_URI+"&response_type=code"
    +"&scope=user_read";

  const handleClick = (e) => {
    e.preventDefault();
    /* if (props.streamer) {
      props.setStreamer(false);
      props.setViewer(true);
    } else {
      props.setViewer(false);
      props.setStreamer(true);
    } */
  }

  const backHome = (e) => {
    e.preventDefault();
    window.location = "http://localhost:3000"
  }

  const clearStorage = (e) => {
    e.preventDefault();
    sessionStorage.clear();
    window.location = "http://localhost:3000"
  }

  return (
    <div className="Header">
      <ElevationScroll>
        <AppBar position="static">
          <Toolbar>
          <img onClick={backHome} src={ logo } width={"20%"} height={"20%"} />.
            <div className={classes.tabContainer}>
              {/* <Tab onClick = {handleClick} label="VIEWER" />
              <Tab onClick = {handleClick} label="STREAMER" /> */}
              <Link to="/wallet"><Tab label="WALLET" /></Link> 
              <Link to="/deposit"><Tab label="DEPOSIT" /></Link>
              <Link to="/streamer"><Tab label="DONATION" /></Link>
              <Link to="/account"><Tab label="ACCOUNT" /></Link>
              { !sessionStorage.username ? 
              <Tab label="GUEST" />:
              <Tab onClick={clearStorage} label="LOGOUT" /> }
            </div>
          </Toolbar>
        </AppBar>
      </ElevationScroll>
    </div>
  );
}

export default Header;
