import { createMuiTheme } from "@material-ui/core/styles";

export default createMuiTheme({
  palette: {
    common: {
      arcBlue: "#0B72B9",
      arcOrange: "#FFBA60"
    },
    primary: {
      main: "#fafafa"
    },
    secondary: {
      main: "#FFBA60"
    }
  },
  typography: {
    h3: {
      fontweight: 300
    }
  }
});
