import React, { Fragment, useState } from "react";
import ReactAudioPlayer from "react-audio-player";
import io from 'socket.io-client';
import { Grid, Typography } from '@material-ui/core';

const socket = io.connect("https://api.cryptoplease.me/")

function DonationPage() {
  const [audioSrc, setAudioSrc] = useState("");
  const [text, setText] = useState("");

  socket.on("donation", function (data) {
    setAudioSrc(audioSrc => "https://cryptodona.s3.ap-northeast-2.amazonaws.com/" + data.url + ".mp3");
    setText(data.text);
    setTimeout(() => {
      setText(text => "");
    }, 12000);
  });  

  return (
    <Fragment>
      { text.length > 2 ?
      <img src = {"https://cryptodona.s3.ap-northeast-2.amazonaws.com/space_dance_nasa.gif"}
          style = {{width:"100%", height: "auto"}} />: null }
      <div><h1>{ text }</h1></div>
      { text.length > 2 ?      
      <ReactAudioPlayer src={audioSrc} autoPlay controls={false} />: null }
    </Fragment>
  );
}

export default DonationPage;
