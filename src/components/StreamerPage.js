import React, { useRef } from "react";
import { TextField, Grid, Typography, Button, Box } from '@material-ui/core';
import Header from '../components/HeaderPage';
import { ThemeProvider } from "@material-ui/styles";
import theme from '../components/ui/Theme';
import blockchainGif from './ui/blockchain.gif';

function StreamerPage(props) {
  const textRef = useRef("");
  const coinRef = useRef("");
  const streamerRef = useRef("");

  const handleClick = (e) => {
    e.preventDefault();
    props.setQuantity(coinRef.current.value);
    props.setText(textRef.current.value);
    props.setSendTo(streamerRef.current.value);
  }

  return (
    <div className="StreamerPage">
      <ThemeProvider theme={theme}>
        <Header />
      </ThemeProvider>
      <Grid container style={{ padding: '3em 5em' }}>
        <Grid item container direction="column" xs={6}>
          <Grid item>
            <Grid
              container
              direction="column"
              justify="space-between"
              style={{ padding: '2em'}}
            >
            <Typography variant="h5">DONATION</Typography>
            </Grid>
            
              <Grid
                container
                direction="column"
                justify="space-between"
                style={{ padding: '2em'}}
              >
                <Typography variant="h6">DONATION AMOUNT</Typography>
                <TextField
                  inputRef={coinRef}
                  onChange={e => {}}
                  placeholder="ex) 15"
                  rows={1}
                  required={true}
                />
              </Grid>
              <Grid
                container
                direction="column"
                justify="space-between"
                style={{ padding: '2em'}}
              >
                <Typography variant="h6">DONATION MESSAGE</Typography>
                <TextField
                  inputRef={textRef}
                  onChange={e => {}}
                  placeholder="Enter a message"
                  fullWidth
                  multiline
                  rows={4}
                  required={true}
                />
              </Grid>
              <Grid
                container
                direction="column"
                justify="space-between"
                style={{ padding: '2em'}}
              >
                <Typography variant="h6">SEND TO (STREAMER ID)</Typography>
                <TextField
                  inputRef={streamerRef}
                  onChange={e => {}}
                  placeholder="ex) "
                  rows={1}
                  required={true}
                />
              </Grid>
              <Grid
                container
                direction="column"
                justify="space-between"
                style={{ padding: '2em'}}
              >
                <Button onClick={handleClick} variant="contained" color="primary">
                  Send
                </Button>
              </Grid>
           
          </Grid>
        </Grid>
        <img src={blockchainGif} />
      </Grid>
    </div>
  );
}

export default StreamerPage;
